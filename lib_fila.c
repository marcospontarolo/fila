#include <stdio.h>
#include <stdlib.h>
#include "lib_fila.h"


int inicializa_fila(t_fila *f){
	t_nodo *sentinela_inicio=NULL, *sentinela_final=NULL;

	/* alocando espaço para as sentinelas */
	sentinela_final = (t_nodo *) malloc(sizeof(t_nodo));
	sentinela_inicio = (t_nodo *) malloc(sizeof(t_nodo));
	/* caso NULL, falta memória */
	if((sentinela_final == NULL) || (sentinela_inicio == NULL)){
		printf("ERRO: Memoria insuficiente.");
		return 0;
	}

	/*inicializa ini,fim,tam e atual da fila para as sentinelas */
	f->ini = sentinela_inicio;
	f->fim = sentinela_final;
	f->atual = NULL;
	f->tamanho = 0;

	/* aponta as sentinelas para elas mesmas, após inicado*/
	sentinela_inicio->prox = sentinela_final;
	sentinela_final->prev = sentinela_inicio;

	return 1;
}

int fila_vazia(t_fila *f){
	if(f->tamanho == 0)
		return 1;
	else
		return 0;
}

int tamanho_fila(t_fila *f){

	/* verifica se a fila de fato existe usando suas sentinelas */
	if((f->tamanho >= 0) && (f->ini != NULL) &&  (f->fim != NULL))
		return f->tamanho;
	else
		return -1;
}

int enfileira(int id, int t, t_fila *f){
	
	t_nodo *novo=NULL;

	novo = (t_nodo *) malloc(sizeof(t_nodo));

	if(novo == NULL){
		printf("ERRO: Memoria insuficiente.");
		return 0;
	}

	novo->prox = f->fim;
	novo->prev = f->fim->prev;
	novo->id = id;
	novo->t = t;

	f->fim->prev->prox = novo;
	f->fim->prev = novo;

	f->tamanho++;

	return 1;	
}

int desenfileira(int *id, int *t, t_fila *f){


	if(fila_vazia(f))
		return 0;

	t_nodo *remover=NULL;
	
	remover = f->ini->prox;

	f->ini->prox->prox->prev = f->ini;
	f->ini->prox = f->ini->prox->prox;

	f->tamanho--;

	*id = remover->id;
	*t = remover->t;

	free(remover);

	return 1;
}

int removefila(int id, int *t, t_fila *f){

	if(fila_vazia(f))
		return 0;

	f->atual = f->ini->prox;
	f->fim->id = id;


	while(f->atual->id != id){
		f->atual = f->atual->prox;
	}

	if(f->atual == f->fim){
		return 0;
	}
	
	else{
		
		t_nodo *remover;

		remover = f->atual;

		f->atual->prev->prox = f->atual->prox;
		f->atual->prox->prev = f->atual->prev;
		f->atual = NULL;

		f->tamanho--;

		*t = remover->t;
		
		free(remover);

		return 1;		
	}

	return 0;
}

void imprime_fila(t_fila *f){
	if(!fila_vazia(f)){

		int id_atual;
		int t_atual;
		int i;
		int tam_fila;

		f->atual = f->ini->prox;

		tam_fila = tamanho_fila(f);

		for(i = 1; i <= tam_fila; i++){

			id_atual = f->atual->id;
			t_atual = f->atual->t;

			printf(" %d(%d)", id_atual, t_atual);

			f->atual = f->atual->prox;

		}

	}
		printf("\n");
}