/*
Lib fila.h
*/

struct t_nodo {
    int id;
    int t;
    struct t_nodo *prox;
    struct t_nodo *prev;
};
typedef struct t_nodo t_nodo;

struct t_fila {
    t_nodo *ini;
    t_nodo *atual;
    t_nodo *fim;
    int tamanho;
};
typedef struct t_fila t_fila;

/*
retorna 1 se inicialou com sucesso a fila ou zero caso contrario.
*/
int inicializa_fila(t_fila *f);

/*
retorna 1 se a fila esta vazia ou zero caso contrario.
*/
int fila_vazia(t_fila *f);

/*
retorna o tamanho da fila.  Se a fila nao existe retorna -1
*/
int tamanho_fila(t_fila *f);

/*
retorna 1 se inseriu com sucesso o id com autonomia de t UT’s no final da fila ou zero casocontrario.
*/
int enfileira(int id, int t, t_fila *f);

/*
retorna 1 se desenfileirou com sucesso o id com autonomia de t UT’s que estava no inicio da fila ou zero caso contrario.
*/
int desenfileira(int *id, int *t, t_fila *f);

/*
retorna 1 se removeu com sucesso o id que tinha autonomia de t UT’s da fila, nao importaa posicao que este id ocupa na fila, ou zero caso contrario
*/
int removefila(int id, int *t, t_fila *f);

/*
imprime a fila caso ela exista, senao nao imprime nada.
*/
void imprime_fila(t_fila *f);

