#include <stdio.h>
#include "lib_fila.h"

void atualiza_fila(t_fila *fila, int *existe_prioridade){

	int j, tam_fila, id_atual, ut_atual;

	tam_fila = tamanho_fila(fila);

	for(j = 1; j <= tam_fila; j++){
			
			desenfileira(&id_atual, &ut_atual, fila);

			/* verifica se ocorreu alguma prioridade */
			if(ut_atual - 1 <= 0){

				*existe_prioridade = id_atual;

			}
			else{

				*existe_prioridade = 0;
		
			}
			/* faz o decremento do UT de cada aviao */
			enfileira(id_atual, ut_atual - 1, fila);
		}
}

void checa_fila(t_fila *fila, int *existe_prioridade){

	int j, tam_fila, id_atual, ut_atual;

	tam_fila = tamanho_fila(fila);

	/* como pode ocorrer mais de uma prioridade por fila, verifica se isso de fato ocorreu */
	for(j = 1; j <= tam_fila; j++){
			
			desenfileira(&id_atual, &ut_atual, fila);

			/* verifica se ocorreu alguma prioridade */
			
			if(ut_atual <= 0){

				existe_prioridade = &id_atual;

			}
			else {

			existe_prioridade = 0;

			}
			enfileira(id_atual, ut_atual, fila);
		}
}

void pousa_prioridades(t_fila *fila, int *status_pista1, int *status_pista2, int *status_pista3, int *caiu, int *tam_vetor, float *pousou, int *sem_combustivel){

	int existe_prioridade, ut_atual;

		atualiza_fila(fila, &existe_prioridade);


		/* caso exista uma prioridade (aviao ficando sem combustivel) pousa imediatamente, se possivel */
		while(existe_prioridade != 0){	

			removefila(existe_prioridade, &ut_atual, fila);

			/* caso o ut seja menor que zero, significa que acabou o combustivel do aviao em pleno ar */
			if(ut_atual < 0){

				(*tam_vetor)++;
				caiu[(*tam_vetor)] = existe_prioridade;

			}
			else{
				/* tenta pousar a prioridade em uma das pistas, comecando pela 3 */	
				if(!*status_pista3){
					*status_pista3 = 1;
					(*pousou)++;
					(*sem_combustivel)++;
				}
				else if (!*status_pista2){
					*status_pista2 = 1;
					(*pousou)++;
					(*sem_combustivel)++;
				}
				else if (!*status_pista1){
					(*pousou)++;
					*status_pista1 = 1;
					(*sem_combustivel)++;
				}
				else {
					
				/* as pistas ja estao ocupadas com pousous prioritarios, entao o aviao cai */
				(*tam_vetor)++;
				caiu[(*tam_vetor)] = existe_prioridade;

				}
			}

			existe_prioridade = 0;
			checa_fila(fila, &existe_prioridade);
		}
}


int main(){
	t_fila fila_aterrisagem1, fila_aterrisagem2, fila_aterrisagem3, fila_aterrisagem4;

	t_fila fila_decolagem1, fila_decolagem2, fila_decolagem3;

	int id, tempo_simulacao,i,j,k, quantidade_aterrisagem, quantidade_decolagem, uts, sem_combustivel, caiu[1000];

	int status_pista1, status_pista2, status_pista3, id_atual, ut_atual, tam_vetor;

	float decolou, pousou, tempoM_aterrisagem, tempoM_decolagem;


	/* inicializa as filas de aterrisagem */
	inicializa_fila(&fila_aterrisagem1);
	inicializa_fila(&fila_aterrisagem2);
	inicializa_fila(&fila_aterrisagem3);
	inicializa_fila(&fila_aterrisagem4);

	/* inicializa as filas de decolagem */
	inicializa_fila(&fila_decolagem1);
	inicializa_fila(&fila_decolagem2);
	inicializa_fila(&fila_decolagem3);

	id = 1;
	sem_combustivel = 0;
	tempoM_decolagem = 0;
	tempoM_aterrisagem = 0;
	pousou = 0;
	decolou = 0;
	tam_vetor = -1;
	/* consulta o tempo de simulacao */
	scanf("%d", &tempo_simulacao);

	/* aplicando a simulacao para o tempo dado em scanf */ 
	for(i = 0; i <= tempo_simulacao; i++){

	/* 
	como 0 eh o ponto inicial, verifica se a simulacao 
	ja acabou, caso sim, imprime o final na tela 
	*/
		if(i != tempo_simulacao){
	

		/* lendo a quantidade de avioes que vao aterrisar */
			scanf("%d", &quantidade_aterrisagem);


		/* guardando seus respectivos UT`s */
		
				for(j = 1; j <= quantidade_aterrisagem; j++){

					scanf("%d", &uts);

				/* procura inserir na fila com menor quantidade de avioes para manter equilibrado */
					if(tamanho_fila(&fila_aterrisagem4) < tamanho_fila(&fila_aterrisagem3)){

						enfileira(2*id+1, uts, &fila_aterrisagem4);
					}
					else if (tamanho_fila(&fila_aterrisagem3) < tamanho_fila(&fila_aterrisagem2)){
						enfileira(2*id+1, uts, &fila_aterrisagem3);
					}
					else if (tamanho_fila(&fila_aterrisagem2) < tamanho_fila(&fila_aterrisagem1)){
						enfileira(2*id+1, uts, &fila_aterrisagem2);
					}
					else
						enfileira(2*id+1, uts, &fila_aterrisagem1);

					id++;

				}

				/* lendo a quantidade de avioes para decolar */
				scanf("%d", &quantidade_decolagem);
			
				/* enfileirando os avioes de decolagem */
				for(j = 1; j <= quantidade_decolagem; j++){

					/* procura inserir na fila com menor quantidade de avioes para manter equilibrado */
					if(tamanho_fila(&fila_decolagem3) < tamanho_fila(&fila_decolagem2)){

						enfileira(2*id, 0, &fila_decolagem3);
					}
					else if (tamanho_fila(&fila_decolagem2) < tamanho_fila(&fila_decolagem1)){
						enfileira(2*id, 0, &fila_decolagem2);
					}
					else
						enfileira(2*id, 0, &fila_decolagem1);

					id++;

				}
			} 

		/* mostra todo o output da simulacao */
		printf("Unidade de tempo %d \n", i);

		printf("fila aterrisagem 1:");
		imprime_fila(&fila_aterrisagem1);

		printf("fila aterrisagem 2:");
		imprime_fila(&fila_aterrisagem2);

		printf("fila aterrisagem 3:");
		imprime_fila(&fila_aterrisagem3);
	
		printf("fila aterrisagem 4:");
		imprime_fila(&fila_aterrisagem4);

		printf("fila decolagem 1:");
		imprime_fila(&fila_decolagem1);

		printf("fila decolagem 2:");
		imprime_fila(&fila_decolagem2);

		printf("fila decolagem 3:");
		imprime_fila(&fila_decolagem3);

		/* calcula os tempos medios */
		if(pousou != 0)
			tempoM_aterrisagem = i / pousou;
		if(decolou !=0)
			tempoM_decolagem = i / decolou;


		printf("tempo medio para aterrissagem: %f \n", tempoM_aterrisagem);
		printf("tempo medio par decolagem: %f \n", tempoM_decolagem);
		printf("numero de aeronaves que aterrissaram sem combustivel: %d \n", sem_combustivel);
		printf("IDs das aeronaves que cairam:");

		for(k = 0; k <= tam_vetor; k++){
			printf(" %d", caiu[k]);
		}

		printf("\n");
		printf("\n");

		/* se foi o ultimo segundo de simulacao, entao o programa finaliza */
		if(i == tempo_simulacao)
			return 0;

		/* realiza de fato a simulacao */

		status_pista1 = 0;
		status_pista2 = 0;
		status_pista3 = 0;

		/* atualiza os UTS dos avioes e verifica se algum esta com prioridade para pousar e pousa na medida do possivel */
		if(!fila_vazia(&fila_aterrisagem1))
			pousa_prioridades(&fila_aterrisagem1, &status_pista1, &status_pista2, &status_pista3, caiu, &tam_vetor, &pousou, &sem_combustivel);
		if(!fila_vazia(&fila_aterrisagem2))
			pousa_prioridades(&fila_aterrisagem2, &status_pista1, &status_pista2, &status_pista3, caiu, &tam_vetor, &pousou, &sem_combustivel);
		if(!fila_vazia(&fila_aterrisagem3))
			pousa_prioridades(&fila_aterrisagem3, &status_pista1, &status_pista2, &status_pista3, caiu, &tam_vetor, &pousou, &sem_combustivel);
		if(!fila_vazia(&fila_aterrisagem4))
			pousa_prioridades(&fila_aterrisagem4, &status_pista1, &status_pista2, &status_pista3, caiu, &tam_vetor, &pousou, &sem_combustivel);

		/* apos pousar todas as prioridades que existiam, pousa os avioes sem prioridades */
		if((!status_pista1) || (!status_pista2)){

			if(!fila_vazia(&fila_aterrisagem1)){
				desenfileira(&id_atual, &ut_atual, &fila_aterrisagem1);
				if (!status_pista1)
					status_pista1 = 1;
				else if (!status_pista2)
					status_pista2 = 1;
				pousou++;	
			}

			if((!fila_vazia(&fila_aterrisagem2))){
				if (!status_pista1){
					status_pista1 = 1;
					desenfileira(&id_atual, &ut_atual, &fila_aterrisagem2);
					pousou++;
				}
				else if (!status_pista2){
					status_pista2 = 1;
					desenfileira(&id_atual, &ut_atual, &fila_aterrisagem2);
					pousou++;
				}
			}

			if((!fila_vazia(&fila_aterrisagem3))){
				if (!status_pista1){
					status_pista1 = 1;
					desenfileira(&id_atual, &ut_atual, &fila_aterrisagem3);
					pousou++;
				}
				else if (!status_pista2){
					status_pista2 = 1;
					desenfileira(&id_atual, &ut_atual, &fila_aterrisagem3);
					pousou++;
				}
			}

			if((!fila_vazia(&fila_aterrisagem4))){
				if (!status_pista1){
					status_pista1 = 1;
					desenfileira(&id_atual, &ut_atual, &fila_aterrisagem4);
					pousou++;
				}
				else if (!status_pista2){
					status_pista2 = 1;
					desenfileira(&id_atual, &ut_atual, &fila_aterrisagem4);
					pousou++;
				}
			}

		}

		/* decola os avioes caso a pista 3 esteja vazia */
		if(!status_pista3){

			if (!fila_vazia(&fila_decolagem1)){
				desenfileira(&id_atual, &ut_atual, &fila_decolagem1);
				status_pista3 = 1;
				decolou++;
			}
			else if (!fila_vazia(&fila_decolagem2)){
				desenfileira(&id_atual, &ut_atual, &fila_decolagem2);
				status_pista3 = 1;
				decolou++;
			}
			else if (!fila_vazia(&fila_decolagem3)){
				desenfileira(&id_atual, &ut_atual, &fila_decolagem3);
				status_pista3 = 1;
				decolou++;
			}
		}

	
	}

	return 0;	
}